package main

import (
	"net/http"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// Echo instance
	app := echo.New()

	// Middleware
	app.Use(middleware.Logger())
	app.Use(middleware.Recover())

	// Routes
	app.GET("/", hello)
	app.GET("/user/:name", user)

	// Start server
	app.Logger.Fatal(app.Start(":1323"))
}

// Handler for /hello
func hello(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]string{
		"message": "Hello, World!",
	})
}

// Handler for /user/:name
func user(c echo.Context) error {
	capitalized := cases.Title(language.English)
	return c.JSON(http.StatusOK, map[string]string{
		"message": "Hello, " + capitalized.String(c.Param("name")) + "!",
	})
}
